import cloudflare from '@sveltejs/adapter-cloudflare';
import Unocss from 'unocss/vite';
import { presetAttributify, presetUno } from 'unocss'
import presetIcons from '@unocss/preset-icons'


/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		adapter: cloudflare(),

		vite: {
			plugins: [
				Unocss({
					presets: [
					  presetAttributify({ /* preset options */}),
					  presetIcons(),
					  presetUno(),
					  // ...custom presets
					],
					shortcuts: {
						// shortcuts to multiple utilities
						'btn': 'py-2 px-4 font-semibold rounded-lg shadow-md',
						'btn-green': 'text-white bg-green-500 hover:bg-green-700',
						// single utility alias
						'red': 'text-red-100'
					  }
				  })
			  ],
		}
	}
};

export default config;
