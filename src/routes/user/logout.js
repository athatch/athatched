import {clearUser} from '$lib/auth.js';

export async function get({ request, platform }) {

  clearUser();

  return {
    status: 302,
    headers:{
      'Location': '/'
      }
  };
}
