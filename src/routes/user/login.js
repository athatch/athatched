import { get } from "svelte/store";
import { kv } from "$lib/stores.js";
import {set as setFlash} from "$lib/flash.js";
import { compare, setUser } from "$lib/auth.js";

export async function post({ request, platform, locals }) {
  const data = await request.formData();
  let user, error;
  const email = data.get("email");
  const password = data.get("password");

  if (!email) error = "Email cannot be blank";

  if (!error) {
    const key = `user:${email}`;
    const KV =  platform ? platform.env.KV : get(kv);
    user = JSON.parse(await KV.get(key));

    if (!user || !compare(password, user.hashedPassword)) {
      error = "Not user with that email or password";
    }
  }

  if (error) {
    // return validation errors
    return {
      status: 400,
      body: { error },
    };
  }

user.msgId = platform ? platform.env.MESSENGER.newUniqueId().toString() : 'no msg id'
  await setUser(user);

 setFlash({info: `Welcome, you have successfully logged in as ${user.email}`})

  // redirect to the newly created item
  return {
    status: 302,
    headers: {
      Location: "/",
    },
  };
}
