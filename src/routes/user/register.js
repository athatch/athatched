import { User } from "$lib/models/user.js";

const valid = function (password) {
  return password ? true : false;
};

export async function post({ request, platform }) {
  const data = await request.formData();
  let user, error;
  const email = data.get("email");
  const password = data.get("password");

  if (!email) error = "Email cannot be blank";
  if (!valid(password)) error = "Your password cannot be blank";

  if (!error) {
    user = await User.add(platform, { email, password });
    if (!user) {
      error = "Sorry a user with that email already exists";
    } 
  }

  if (error) {
    // return validation errors
    return {
      status: 400,
      body: { error },
    };
  }

  // redirect to the newly created item'
  return {
    status: 200,
    body: { user: user.data },
    headers: {},
  };
}
