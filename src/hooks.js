


export async function handle({ event, resolve }) {
  const response = await resolve(event);
  return response;
}

export function getSession(event) {
  //TODO maybe pair down user record?
  const ses = {user: event && event.locals && event.locals.user ? event.locals.user : null}
  return ses;
}
