import {devKV} from '$lib/devKV.js';

export class Model {
  constructor(platform) {
    this.platform = platform;
    this.store = platform ? platform.KV : devKV();
    this.storePrefix = `${this.constructor.name}`;
    this.data = {};
  }

  key() {
    const idPart = this.data[this.constructor.idName];
    return idPart ? `${this.storePrefix}:${idPart}` : false;
  }

  set(data) {
    this.data = data;
    return this.data;
  }

  update(data) {
    this.set({ ...this.data, ...data });
    return this.data;
  }

  async exists() {
    const key = this.key();
    const existing = await this.store.get(key);
    return existing ? true : false;
  }

  async save() {
    const key = this.key();
    if (!key)
      throw `Cannot save ${this.storePrefix} without a data field called '${this.idName}'`;
    await this.store.put(key, JSON.stringify(this.data));
    return true;
  }

 static async add(kvStore, data) {
    const model = new this();
    model.set(data);
    const exists = await model.exists();
    if (exists) return false;
    await model.save();
    return model;
  }
}
